## Training/Evaluation Dataset Retrieval
Both the training and the evaluation datasets will be available for download in the [Downloads](https://bitbucket.org/ipapadakis/eswc2016-challenge/downloads) section. However, if you want to build them, you are able to do so by following the instructions in the section below.

### Training/Evaluation Dataset Building from DBpedia SPARQL Benchmark
Here we describe the process of building both the training and the evaluation dataset in Ubuntu 14.04. Along these lines, we provide scripts that download the 10% dataset and the 100% dataset of the "DBpedia SPARQL Benchmark" (http://benchmark.dbpedia.org/) and accordingly transform them to the final training and evaluation datasets.

Before executing any of the provided shell scripts, make sure to install their dependencies:

    sudo apt-get install wget python-librdf

Afterwards, execute the shell scripts:

    ./make_training.sh
    ./make_evaluation.sh

Depending on the internet connection and the computer speed, the first script should complete within 20 minutes, while the second could take up a few hours (about 4 hours). The output datasets are stored in the "datasets" directory.

