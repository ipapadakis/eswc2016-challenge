#!/bin/bash
# Downloads and creates the training dataset.

if [ -d "temp" ]
then
    echo "Temporary folder temp exists."
else
    mkdir temp
fi

if [ -d "datasets" ]
then
    echo "datasets folder exists."
else
    mkdir datasets
fi

cd temp

echo "Checking availability of source dataset..."

if [ -f "benchmark_10.nt" ]
then
    echo "File benchmark_10.nt was found in temp."

elif [ -f "benchmark_10.nt.bz2" ]
then

    echo "File benchmark_10.nt.bz2 was found in temp."

    # Extract dataset.
    echo "Extracting dataset..."
    bunzip2 benchmark_10.nt.bz2

else
    # Download dataset.
    echo "Downloading benchmark_10.nt.bz2."
    wget ./temp/benchmark_10.nt.bz2 http://benchmark.dbpedia.org/benchmark_10.nt.bz2

    # Extract dataset.
    echo "Extracting dataset..."
    bunzip2 benchmark_10.nt.bz2
fi

echo "Filtering dataset..."

cd ..

#  Remove bnodes and untyped nodes from benchmark_10.nt (training dataset).
python filter_dataset.py ./temp/benchmark_10.nt

# Move output datasets to the datasets directory.
mv ./temp/benchmark_10.nt.filtered ./datasets/training_dataset.nt

echo "Done!"
