# -*- coding: utf-8 -*-
"""
This program filters untyped nodes and blank nodes (and their edges) out of the input N-triples file and saves the
filtered file under the same directory.

:author: Spyridon Kazanas
:contact: s.kazanas@gmail.com
"""
import RDF
import argparse
import os
import sys
import datetime

rdf_type = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"
typed = set()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                     epilog='Example: filter_dataset.py ./input.nt')

    parser.add_argument('inputfile',
                        type=str,
                        metavar='INPUT',
                        help='N-Triples file to be read.')

    args = parser.parse_args()

    # Process command line arguments
    input_file = os.path.abspath(os.path.expanduser(args.inputfile))
    output_file = input_file+".filtered"

    # Test input file existence
    if not os.path.isfile(input_file):
        print "Input file not found: ", input_file
        sys.exit('-1')

    time_start = datetime.datetime.now()

    # First Pass: Type dictionary creation.
    input_RDF_triples = 0
    output_RDF_triples = 0
    with open(output_file, 'w') as f:
        print "First Pass: Building the set of typed nodes."
        for triple in RDF.NTriplesParser().parse_as_stream("file:" + input_file):

            # Increase the counter.
            input_RDF_triples += 1

            if str(triple.predicate) == rdf_type and not triple.subject.is_blank() and not triple.object.is_blank():
                s, p, o = (str(triple.subject), str(triple.predicate), str(triple.object))
                typed.add(s)

                # Increase the counter.
                output_RDF_triples += 1

                # Write the node-typing triples
                f.write(str(triple) + " .\n")

        # Second Pass: Graph creation.
        print "Second Pass: Filtering typed and blank nodes."
        for triple in RDF.NTriplesParser().parse_as_stream("file:" + input_file):
            if str(triple.predicate) != rdf_type and not triple.object.is_blank() and not triple.subject.is_blank():
                s, p, o = (str(triple.subject), str(triple.predicate), str(triple.object))
                if s in typed:
                    if triple.object.is_literal():

                        # Increase the counter.
                        output_RDF_triples += 1

                        # Write the literal-connecting triples
                        f.write(str(triple) + " .\n")

                    elif o in typed:

                        # Increase the counter.
                        output_RDF_triples += 1

                        # Write the node-connecting triples
                        f.write(str(triple) + " .\n")

    # Report
    print "REPORT"
    print "    Total Triples of INPUT RDF dataset:", input_RDF_triples
    print "    Total Triples of OUTPUT filtered RDF dataset:", output_RDF_triples
    print ""
    print "    The OUTPUT filtered RDF dataset is", round(float(input_RDF_triples) / float(output_RDF_triples),
                                             2), "times smaller than the INPUT RDF dataset."
    print " Created filtered dataset: " + output_file
    time_duration = datetime.datetime.now() - time_start
    print "Total time: ", time_duration.total_seconds(), "sec"
